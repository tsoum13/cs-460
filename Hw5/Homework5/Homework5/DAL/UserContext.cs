﻿using Homework5.Models
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Homework5.DAL
{
    public class UserContext : DbContext
    {

        public DbSet<User> Users { get; set; }  
    }
}