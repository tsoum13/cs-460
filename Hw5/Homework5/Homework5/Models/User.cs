﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Homework5.Models
{
    public class User
    {
        [Display(Name = "First_______Name"), Required]
        public string FirstName { get; set; }

        [Display(Name = "Last_______Name"), Required]
        public string LastName { get; set; }

        [Display(Name = "Date:"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}"), Required]
        public DateTime date { get; set; }

        [Display(Name = "Phone_Number: "), DisplayFormat(DataFormatString = "{0:###-###-####}"), Required]
        public long phoneNum { get; set; }


        [Display(Name = "Catalog Year:"), Required]
        public int catalogYear { get; set; }

        [Display(Name = "V#:"), DisplayFormat(DataFormatString = "{0:D8}"), Required]
        public int vId { get; set; }

        [Display(Name = "Email:"), Required]
        public string email { get; set;}

        [Display(Name = "Major:"), Required]
        public string major { get; set; }

        [Display(Name = "Minor:"), Required]
        public string minor { get; set; }

        [Display(Name = "Advisor:"), Required]
        public string advisor { get; set;}

    public override string ToString()   
    {
        return $"{base.ToString()}: {LastName} {FirstName} {date} {phoneNum} {catalogYear} vId = {"V#" + vId} {email} {major} {minor} {advisor} ";

    }


    }




}