﻿using Homework5.DAL;
using Homework5.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Homework5.Controllers
{
    public class UserController : Controller
    {

       // static private DAL.UserCollection users = new DAL.UserCollection();

        private UserContext db = new UserContext();
      
        // GET: List all the users
        public ActionResult Index()
        {
           // return View(users.theUsers);

            return View(db.Users.ToList());
        }

        //GET: PAge with Form to create  a new user and add them to the system
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                Debug.WriteLine("User in Create [POST]: " + user);
               // users.theUsers.Add(user);
              //  Debug.WriteLine(users.theUsers);
                //users.theUsers.Add(user);
                db.Users.Add(user);
              //  db.SaveChanges();
                 //Debug.WriteLine(users.theUsers);
                 // use the object
                 return RedirectToAction("Index");   // return a HTTP 302
            }
            return View(user);

        }
    }
}