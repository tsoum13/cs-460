﻿
--#####Pirates#######
INSERT INTO [dbo].[Pirate](PirateName, ShanghaiDate)VALUES ('Kirkland', '11/25/2016');
INSERT INTO [dbo].[Pirate](PirateName, ShanghaiDate)VALUES ('Hamilton', '10/15/2016');
INSERT INTO [dbo].[Pirate](PirateName, ShanghaiDate)VALUES ('Beach', '05/15/2016');
INSERT INTO [dbo].[Pirate](PirateName, ShanghaiDate)VALUES ('Dell', '01/16/2016');
INSERT INTO [dbo].[Pirate](PirateName, ShanghaiDate)VALUES ('Coffee', '09/16/2016');

--#####Ship#######	
INSERT INTO [dbo].[Ship](ShipName, ShipType, Tonnage)VALUES ('Costco', 'Big', 10000);
INSERT INTO [dbo].[Ship](ShipName, ShipType, Tonnage)VALUES ('Crossaint', 'Small', 5000);
INSERT INTO [dbo].[Ship](ShipName, ShipType, Tonnage)VALUES ('Boomer', 'Medium', 8000);
INSERT INTO [dbo].[Ship](ShipName, ShipType, Tonnage)VALUES ('Duh', 'Big', 15000);
INSERT INTO [dbo].[Ship](ShipName, ShipType, Tonnage)VALUES ('Chair', 'Small', 4000);


--#####Crew#######
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (1,1, 500.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (1,1, 300.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (1,1, 800.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (1,1, 500.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (2,2, 600.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (2,2, 0.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (2,2,400.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (2,2,100.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (3,3,200.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (3,3,500.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (3,3,1000.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (3,3,60.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (4,4,800.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (4,4,700.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (4,4,30.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (4,4,80.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (5,5,700.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (5,5,20.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (5,5,90.00);
INSERT INTO [dbo].[Crew](ShipID, PirateID,BootyValue)VALUES (5,5,60.00);
