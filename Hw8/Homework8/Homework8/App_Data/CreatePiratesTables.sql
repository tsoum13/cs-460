﻿IF OBJECT_ID('dbo.Crew', 'U') IS NOT NULL
	DROP TABLE [dbo].[Crew];
GO

IF OBJECT_ID('dbo.Pirate', 'U') IS NOT NULL
	DROP TABLE [dbo].[Pirate];
GO

IF OBJECT_ID('dbo.Ship', 'U') IS NOT NULL
	DROP TABLE [dbo].[Ship];
GO

CREATE TABLE Pirate
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[PirateName] NVARCHAR (50) NOT NULL,
	[ShanghaiDate] Date NOT NULL,
	CONSTRAINT [PK_dbo.Pirate] PRIMARY KEY CLUSTERED ([ID] ASC)
);

CREATE TABLE Ship
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[ShipName] NVARCHAR(50) NOT NULL,
	[ShipType] NVARCHAR(75) NOT NULL,
	[Tonnage] int NOT NULL,
	CONSTRAINT [PK_dbo.Ship] PRIMARY KEY CLUSTERED ([ID] ASC),
	
);

CREATE TABLE Crew
(	
	[ID] INT IDENTITY (1,1) NOT NULL,
	[PirateID] INT NOT NULL REFERENCES Pirate ([ID]),
	[ShipID] INT NOT NULL REFERENCES Ship ([ID]),
	[BootyValue] decimal (7,2) NOT NULL,
	CONSTRAINT [PK_dbo.Crew] PRIMARY KEY CLUSTERED ([ID] ASC),
	CONSTRAINT [FK_dbo.Crew_dbo.Pirate_ID] FOREIGN KEY	([PirateID]) REFERENCES [dbo].[Pirate] ([ID]),
	CONSTRAINT [FK_dbo.Crew_dbo.Ship_ID] FOREIGN KEY ([ShipID]) REFERENCES [dbo].[Ship] ([ID]),
			
);