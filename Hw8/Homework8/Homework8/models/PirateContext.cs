namespace Homework8.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PirateContext : DbContext
    {
        public PirateContext()
            : base("name=PirateContext")
        {
        }

        public virtual DbSet<Crew> Crews { get; set; }
        public virtual DbSet<Pirate> Pirates { get; set; }
        public virtual DbSet<Ship> Ships { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Crew>()
                .Property(e => e.BootyValue)
                .HasPrecision(7, 2);

            modelBuilder.Entity<Pirate>()
                .HasMany(e => e.Crews)
                .WithRequired(e => e.Pirate)
                .HasForeignKey(e => e.PirateID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pirate>()
                .HasMany(e => e.Crews1)
                .WithRequired(e => e.Pirate1)
                .HasForeignKey(e => e.PirateID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ship>()
                .HasMany(e => e.Crews)
                .WithRequired(e => e.Ship)
                .HasForeignKey(e => e.ShipID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ship>()
                .HasMany(e => e.Crews1)
                .WithRequired(e => e.Ship1)
                .HasForeignKey(e => e.ShipID)
                .WillCascadeOnDelete(false);
        }
    }
}
