﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Homework8.Models;

namespace Homework8.Controllers
{
    public class CrewController : Controller
    {
        private PirateContext db = new PirateContext();

        // GET: Crew
        public ActionResult Index()
        {
            var crews = db.Crews.Include(c => c.Pirate).Include(c => c.Ship);
            return View(crews.ToList());
        }

        // GET: Crew/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Crew crew = db.Crews.Find(id);
            if (crew == null)
            {
                return HttpNotFound();
            }
            return View(crew);
        }

        // GET: Crew/Create
        public ActionResult Create()
        {
            ViewBag.PirateID = new SelectList(db.Pirates, "ID", "PirateName");
          ViewBag.PirateID = new SelectList(db.Pirates, "ID", "PirateName");
           ViewBag.ShipID = new SelectList(db.Ships, "ID", "ShipName");
            ViewBag.ShipID = new SelectList(db.Ships, "ID", "ShipName");
            return View();
        }

        // POST: Crew/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,PirateID,ShipID,BootyValue")] Crew crew)
        {
            if (ModelState.IsValid)
            {
                db.Crews.Add(crew);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PirateID = new SelectList(db.Pirates, "ID", "PirateName", crew.PirateID);
           ViewBag.PirateID = new SelectList(db.Pirates, "ID", "PirateName", crew.PirateID);
          ViewBag.ShipID = new SelectList(db.Ships, "ID", "ShipName", crew.ShipID);
            ViewBag.ShipID = new SelectList(db.Ships, "ID", "ShipName", crew.ShipID);
            return View(crew);
        }

        // GET: Crew/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Crew crew = db.Crews.Find(id);
            if (crew == null)
            {
                return HttpNotFound();
            }
            ViewBag.PirateID = new SelectList(db.Pirates, "ID", "PirateName", crew.PirateID);
           ViewBag.PirateID = new SelectList(db.Pirates, "ID", "PirateName", crew.PirateID);
           ViewBag.ShipID = new SelectList(db.Ships, "ID", "ShipName", crew.ShipID);
            ViewBag.ShipID = new SelectList(db.Ships, "ID", "ShipName", crew.ShipID);
            return View(crew);
        }

        // POST: Crew/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,PirateID,ShipID,BootyValue")] Crew crew)
        {
            if (ModelState.IsValid)
            {
                db.Entry(crew).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PirateID = new SelectList(db.Pirates, "ID", "PirateName", crew.PirateID);
         ViewBag.PirateID = new SelectList(db.Pirates, "ID", "PirateName", crew.PirateID);
          ViewBag.ShipID = new SelectList(db.Ships, "ID", "ShipName", crew.ShipID);
            ViewBag.ShipID = new SelectList(db.Ships, "ID", "ShipName", crew.ShipID);
            return View(crew);
        }

        // GET: Crew/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Crew crew = db.Crews.Find(id);
            if (crew == null)
            {
                return HttpNotFound();
            }
            return View(crew);
        }

        // POST: Crew/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Crew crew = db.Crews.Find(id);
            db.Crews.Remove(crew);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
