﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Homework6.Models;
using System.Net;

namespace Homework6.Controllers
{
    public class ProductsController : Controller
    {
        // GET: Products
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// shows subcategories
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult SubCategory(int? id)
        {
            if (id == null)
            {
                id = 1;
            }

            using (var db = new ProductsContext())
            {
                var ProdCategory = db.ProductCategories.Where(x => x.ProductCategoryID == id).First();

                ViewBag.Title = ProdCategory.Name;

                return View(ProdCategory.ProductSubcategories.ToList());
              }
        }
        /// <summary>
        /// to get sub products to the view
        /// </summary>
        public ActionResult ViewProducts(int? id)
        {

            if (id == null)
            {
                id = 1;
            }
            using (var db = new ProductsContext())
            {

                var SubCategory = db.ProductSubcategories.Where(sub => sub.ProductSubcategoryID == id).First();

                ViewBag.Title = SubCategory.Name;
                ///do view - becuz its a view type
                return View(SubCategory.Products.ToList());
            }
                            
        }
        /// <summary>
        /// to create a description for the product
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DescriptionProduct(int? id)
        {
            int pid = id ?? 0;
            if (pid == 0)
            {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            using (var db = new ProductsContext())
            {

                Product currentProduct = db.Products.Where(x => x.ProductID == pid).First();
                ProductModelProductDescriptionCulture productDescriptionAndCulture = db.ProductModelProductDescriptionCultures.Where(x => x.ProductModelID == currentProduct.ProductModelID).First();
                ProductDescription descriptionObject = db.ProductDescriptions.Where(x => x.ProductDescriptionID == productDescriptionAndCulture.ProductDescriptionID).First();


                ViewBag.productName = currentProduct.Name;
                ViewBag.currentReviews = db.ProductReviews.Where(x => x.ProductID == currentProduct.ProductID).ToList();

                ///add review
                ProductReview addReview = db.ProductReviews.Create();
                addReview.ProductID = currentProduct.ProductID;
                addReview.ProductReviewID = db.Products.Count() + 1;

                ///add current date time
                addReview.ReviewDate = addReview.ModifiedDate = DateTime.Now;
                return View(addReview);


                ///Product item = (Product)db.Products.Where(prod => prod.ProductID == id).First();
               /// ProductModelProductDescriptionCulture pmpdc = (ProductModelProductDescriptionCulture)db.ProductModelProductDescriptionCultures.Where(x => x.ProductModelID == item.ProductModelID).First();
               // ProductDescription details = (ProductDescription)db.ProductDescriptions.Where(x => x.ProductDescriptionID == pmpdc.ProductDescriptionID).First();

               /// return View(details);
               }
        }

        [HttpPost]
        public ActionResult DescriptionProduct(ProductReview addReview)
        {
            using (var db = new ProductsContext())
            {
                ///if there is a new review and the validation on the model matches.
                if (ModelState.IsValid)
                {
                    db.ProductReviews.Add(addReview);
                    db.SaveChanges();
                    return RedirectToAction("DescriptionProduct", new { id = addReview.ProductID });
                }

                return RedirectToAction("Index");
            }

        }
    }
}
