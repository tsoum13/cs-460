﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Homework6.Models;

namespace Homework6.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }


        //GET: Products stuff
        public ActionResult Products()
        {
            using (var db = new ProductsContext())
            {
                return View(db.ProductCategories.ToList());
            }
        }

    }


}