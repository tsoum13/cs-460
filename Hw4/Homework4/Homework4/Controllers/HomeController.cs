﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Homework4.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        /// <summary>
        /// just shows the main page for hmwk 4
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            //return "Hello MVC"; example in class
            return View();
        }
        /// <summary>
        /// gets the person's name
        /// </summary>
        /// <returns></returns>
        //GET: Page1 - dynamic data on server side to show the client.
        [HttpGet]
        public ActionResult Page1()
        {
            var FirstName = Request.QueryString["FirstName"];
            var LastName = Request.QueryString["LastName"];
         
            return View();
        }
        /// <summary>
        /// this method returns the person's name in a awkward manner..with a new view Page called "whoa"
        /// </summary>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Page1(string FirstName, string LastName)
        {

            ViewBag.message = ("Wow i didn't know there was a name like that out there, " + FirstName + " " + LastName + "  you must be pretty special");
                

            return View("Whoa");
        }

        /// <summary>
        /// change password
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Page2()
        {

            return View();
        }
        /// <summary>
        /// returns the password
        /// </summary>
        /// <returns> view bag message in new view called password</returns>
        [HttpPost]
        public ActionResult Page2(FormCollection form)
        {

            ViewBag.message = "ok just to recap your name is  " + form["Name"] + " and you wanted to change your password, so your old password is:  " + form["OldPassword"] + " and now your new password is...  " + form["NewPassword"];
            return View("Password");
        }

        public ActionResult Page3()
        {

            return View();

        }

    }
}