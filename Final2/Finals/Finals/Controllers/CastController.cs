﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finals.Models;

namespace Finals.Controllers
{
    public class CastController : Controller
    {
        private FinalsContext db = new FinalsContext();
        // GET: Cast
        public ActionResult Index()
        {
            return View(db.Casts.ToList());
        }
    }
}