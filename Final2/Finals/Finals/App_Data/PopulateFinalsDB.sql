﻿--#####Movie#######
INSERT INTO [dbo].[Movie](Title, Year, Director)VALUES ('Rogue One: A Star Wars Story', '2016', 'Gareth Edwards');
INSERT INTO [dbo].[Movie](Title, Year, Director)VALUES ('Doctor Strange', '2016', 'Scott Derrickson');
INSERT INTO [dbo].[Movie](Title, Year, Director)VALUES ('The Imitation Game', '2014', 'Morten Tyldum');
INSERT INTO [dbo].[Movie](Title, Year, Director)VALUES ('The Theory of Everything', '2014', 'James Marsh');

--#####Actor#######	
INSERT INTO [dbo].[Actor](Name)VALUES ('Felicity Jones');
INSERT INTO [dbo].[Actor](Name)VALUES ('Mads Mikkelsen');
INSERT INTO [dbo].[Actor](Name)VALUES ('Benedict Cumberbatch');
INSERT INTO [dbo].[Actor](Name)VALUES ('Rachel McAdams');

--#####Director#######
INSERT INTO [dbo].[Director](Name)VALUES ('Gareth Edwards');
INSERT INTO [dbo].[Director](Name)VALUES ('Scott Derrickson');
INSERT INTO [dbo].[Director](Name)VALUES ('Morten Tyldum');
INSERT INTO [dbo].[Director](Name)VALUES ('James Marsh');


--#####Cast#######	
INSERT INTO [dbo].[Cast](ActorID, MovieID)VALUES (1, 1);
INSERT INTO [dbo].[Cast](ActorID, MovieID)VALUES (2, 1);
INSERT INTO [dbo].[Cast](ActorID, MovieID)VALUES (3, 2);
INSERT INTO [dbo].[Cast](ActorID, MovieID)VALUES (4, 2);
INSERT INTO [dbo].[Cast](ActorID, MovieID)VALUES (3, 3);
INSERT INTO [dbo].[Cast](ActorID, MovieID)VALUES (1, 4);
INSERT INTO [dbo].[Cast](ActorID, MovieID)VALUES (2, 2);

