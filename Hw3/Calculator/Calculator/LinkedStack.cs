﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class LinkedStack : StackADT
    {
        private Node top;

        /// <summary>
        /// linked stack method
        /// </summary>
        public LinkedStack()
        {
            this.top = null;
            //empty stack condition
        }

        /// <summary>
        /// push method
        /// </summary>
        /// <param name="newItem"></param>
        /// <returns></returns>
        public virtual object Push(object newItem)
        {
            if ((newItem == null))
            {
                return null;
            }

            Node newNode = new Node(newItem, this.top);
            this.top = newNode;
            return newItem;
        }
        /// <summary>
        /// pop method
        /// </summary>
        /// <returns></returns>
        public virtual object Pop()
        {
            if (this.IsEmpty)
            {
                return null;
            }

            object topItem = this.top.data;
            this.top = this.top.next;
            return topItem;
        }
        /// <summary>
        /// peek method
        /// </summary>
        /// <returns>top data of stack</returns>
        public virtual object Peek()
        {
            if (this.IsEmpty)
            {
                return null;
            }

            return this.top.data;
        }
        /// <summary>
        /// checks to see if stack is empty or not
        /// </summary>
        public virtual bool IsEmpty
        {
            get
            {
                return (this.top == null);
            }
        }
        /// <summary>
        /// clears top of stack
        /// </summary>
        public virtual void Clear()
        {
            this.top = null;
        }













    }

}
