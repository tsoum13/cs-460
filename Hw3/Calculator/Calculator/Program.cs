﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Calculator
{
    class Calculator
    {
        /// <summary>
        /// Our Data Structure, used to hold operands for the postfix calculations
        /// </summary>
        private StackADT stack = new LinkedStack();

        /// <summary>
        /// Scanner to get input from the user from the command line
        /// </summary>
        //private Scanner scin = new Scanner(System.in);
        
        /// <summary>
        ///  Entry point method. Disregards any command line arguments.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //Instantiate a "Main" Object so we don't have to make everything static
            Calculator app = new Calculator();
            bool playAgain = true;
            Console.WriteLine("\nPostfix Calculator. Recognizes these operators: + - * /");
            while (playAgain)
            {
                playAgain = app.doCalculation();
             }

            Console.WriteLine("Buh Bye.");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns> boolean value</returns>
        private bool doCalculation()
        {
            Console.WriteLine("Please enter q to quit \n");
            string input = "2 2 +";
            Console.Write("> ");
            // prompt user
            input = Console.ReadLine();
            //  looks like nextLine() blocks for input when used on an InputStream (System.in).  Docs don't say that!
            //  See if the user wishes to quit
            if ((input.StartsWith("q", StringComparison.Ordinal) || input.StartsWith("Q", StringComparison.Ordinal)))
            {
                return false;
            }

            //Goes ahead with the calucation
            string output = "4";
            try
            {
                output = this.evaluatePostFixInput(input);
            }
            catch (System.ArgumentException e)
            {
                output = e.Message;

            }

            Console.WriteLine(("\n\t>>> " + (input + (" = " + output))));
            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public virtual string evaluatePostFixInput(string input)
        {
            if (((string.ReferenceEquals(input, null)) || input.Equals("")))
            {
                throw new System.ArgumentException("Null or the empty string are not valid postfix expressions.");
            }

            this.stack.Clear();
            string s;
            //  Temporary variable for token read
            double a;
            //  Temporary variable for operand
            double b;
            //  ...for operand
            double c;
            //  ...for answer
            //  Scanner st = new Scanner(input);
            input = Console.ReadLine();

            while (input != null)
            {
                if (input == Console.ReadLine()  )// hasNextDouble()
                {
                    this.stack.Push(new double?(input = Console.Read()));//st.nextDouble
                    //  if it's a number push it on the stack
                }
                else
                {
                    //  Must be an operator or some other character or word.
                    input = Console.ReadLine();//st.ReadNext()
                    if ((input.Length > 1))
                    {
                        throw new System.ArgumentException(("Input Error: " + (s + " is not an allowed number or operator")));
                    }

                    if (this.stack.Empty)
                    {
                        throw new System.ArgumentException("Improper input format. Stack became empty when expecting second operand.");
                    }

                    b = ((double?)(this.stack.Pop())).Value;
                    if (this.stack.IsEmpty)
                    {
                        throw new System.ArgumentException("Improper input format. Stack became empty when expecting first operand.");
                    }

                    a = ((double?)(this.stack.Pop())).Value;
                    //  Wrap up all operations in a single method, easy to add other binary operators this way if desired
                    c = this.doOperation(a, b, s);
                    //  push the answer back on the stack
                    this.stack.Push(new double?(c));
                }

            }

            //  End while
            return ((double?)(this.stack.Pop())).ToString();
        }

        public virtual double doOperation(double a, double b, string s)
        {
            double c = 0;
            if (s.Equals("+"))
            {
                c = (a + b);
            }
            else if (s.Equals("-"))
            {
                c = (a - b);
            }
            else if (s.Equals("*"))
            {
                c = (a * b);
            }
            else if (s.Equals("/"))
            {
                try
                {
                    c = (a / b);
                    if (((c == double.NegativeInfinity) || (c == double.PositiveInfinity)))
                    {
                        throw new System.ArgumentException("Can\'t divide by zero");
                    }

                }
                catch (ArithmeticException e)
                {
                    throw new System.ArgumentException(e.Message);
                }

            }
            else
            {
                throw new System.ArgumentException(("Improper operator: " + (s + ", is not one of +, -, *, or /")));
            }

            return c;
        }
        //end calc class



    }
}
