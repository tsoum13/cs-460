﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Node
    {
        public Object data;//The payload
        public Node next;// reference to the next node in the chain

        /// <summary>
        /// initiates the current node as null
        /// </summary>
        public Node()
        {
            data = null;
            next = null;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data">receives data of object</param>
        /// <param name="next">recieves node next</param>
        public Node(Object data, Node next)
        { 
            this.data = data;
            this.next = next;
        }

    }
}
